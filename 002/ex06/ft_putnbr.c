#include <unistd.h>
#include <stdlib.h>

void ft_putchar(char c)
{
	write(1, &c, 1);
}

void ft_putstr(char *str)
{
	int i;

	i = -1;
	while (str[++i] != '\0')
	{
		ft_putchar(str[i]);
	}
}

int len_nb(int nb)
{
	int len;

	len = 0;
	while(nb > 0)
	{
		nb /= 10;
		len++;
	}
	return (len);
}

void ft_putnbr(int nb)
{
	int len;
	char *num_str;

	len = len_nb(nb) + 1;
	num_str = malloc(sizeof(char) * len);
	num_str[len - 1] = '\0';

	while(--len  > 0)
	{
		num_str[len - 1] = (nb % 10) + '0';
		nb = nb / 10;
	}
	ft_putstr(num_str);
}


int main()
{
	int nb;

	nb = 42;
	ft_putnbr(nb);
	return (0);
}
