#include <unistd.h>
#include <stdio.h>

void ft_putchar(char c)
{

	write(1, &c, sizeof(c));
}

int ft_strcmp(char *str1, char *str2)
{

	int i;
	i = 0;
	while(str1[i] && str2[i])
	{
		if (str1[i] != str2[i])
			return (str1[i] - str2[i]);
		i++;
	}
	if ((str1[i] == '\0' && str2[i] != '\0') || (str1[i] != '\0' && str2[i] == '\0'))
		return (str1[i] - str2[i]);
	return 0;
}

void swatch_num (char *str1, char *str2)
{
	char *tmp;

	*tmp = *str1;
	*str1 = *str2;
	*str2 = *tmp;
}

void sort_arguments(int n, char **array)
{
	int i;

	i = 1;
	while(i++ < n)
	{
		if (i + 1 < n && ft_strcmp(array[i], array[i + 1]) > 0)
			swatch_num(array[i], array[1 + i]);
	}
}

 void display_argument(int argc, char **array)
{
	int i;
	int j;

	i = 0;
	while (i++ < argc - 1)
	{
		j = 0;
		while (array[i][j] != '\0')
		{
			ft_putchar(array[i][j]);
			j++;
		}
		ft_putchar('\n');
	}

}

int main( int argc, char **argv)
{

	sort_arguments(argc, argv);
	display_argument(argc, argv);

	return 0;


}
