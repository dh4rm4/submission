#include <stdio.h>

void ft_ultimate_div_mod(int *a, int *b)
{
	int tmp_a;

	tmp_a = *a;
	*a = *a / *b;
	*b = tmp_a % *b;
	printf("%d %d", *a, *b);
}

int main()
{
	int *a, *b;
	int c, d;

	c = 27;
	d = 9;
	a = &c;
	b = &d;
	ft_ultimate_div_mod(a , b);

    return (0);

}
