#include <unistd.h>
#include <stdio.h>

void ft_div_mod(int a, int b, int *div, int *mod)
{
	int c, d;

	c = a / b;
	d = a % b;
	div = &c;
	mod = &d;
	printf("first %d %d\n", *div, *mod);
}

int main()
{
	int a, b;
	int *div, *mod;

	a = 100;
	b = 20;
	ft_div_mod(a, b, div, mod);
//	printf("second %d %d", *div, *mod);
	return (0);
}
