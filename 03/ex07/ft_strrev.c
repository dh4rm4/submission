#include <stdio.h>


char  *ft_strrev(char *str)
{
	int len, i, tmp;

	i = 0;
	len = 0;

	while(*(str + len) != '\0')
	{
		len++;
	}

	while(i < len / 2)
	{
		tmp = str[i];
		str[i] = str[len - i - 1];
		str[len - i - 1] = tmp;
		++i;
	}

	str[len] = '\0';
	return str;
}


int main()
{
	char str[10] = "xibao";
	ft_strrev(str);
	printf("return value1 %s\n", str);
	//printf("value 2 %s\n", ft_strrev(str));
	//printf("value %s\n", ft_strrev(str));
	printf("value %s\n", ft_strrev(str));
	return (0);
}
