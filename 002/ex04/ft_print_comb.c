#include <unistd.h>

void ft_putchar(int i, int j, int k)
{
	char num[4];
	num[0] = i + '0';
	num[1] = j + '0';
	num[2] = k + '0';
	num[3] = ',';
	write(1, num , sizeof(num) );

}


void ft_print_comb(void)
{
	int i, j, k;
	i = 0;
	j = 1;
	k = 2;
	while(i <= 7)
	{
		while(j <= 8)
		{
			while(k <= 9)
			{
				ft_putchar(i, j, k);
				k++;
			}
			j++;
			k = j + 1;
		}
		i++;
		j = i + 1;
		k = j + 1;
	}

}


int main()
{
	ft_print_comb();
	return (0);
}
