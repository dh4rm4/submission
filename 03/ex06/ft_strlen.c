#include <stdio.h>

int ft_strlen(char *str)
{
	int len;

	len = 0;
	while(*(str + len) != '\0')
	{
		++len;
	}

	return (len + 1);


}

int main()
{
	char *str;
	int len;

	str = "xibao";

	len = ft_strlen(str);
	printf("%d", len);

}
