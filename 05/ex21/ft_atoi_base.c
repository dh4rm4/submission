#include <unistd.h>
#include <stdio.h>

int first_char_is_math_sign(int i, int starting_index, char *str);
int current_char_is_not_math_sign(char c);

int get_base_length(char *base)
{
	int base_length;
	int i, j;

	base_length = 0;
	if(!base)
		return 0;

	while(base[base_length])
	{
		if((base[base_length] == '+') || (base[base_length] == '-'))
			return 0;
		j = base_length + 1;
		while(base[j])
		{
			if (base[base_length] == base[j])
				return 0;
			j++;
		}
		base_length++;
	}
	if (base_length < 2)
		return 0;
	return (base_length);
}

int get_starting_index(char *str)
{
	int i;

	i = 0;
	while(str[i] && (str[i] == ' ' || str[i] == '\n'
								|| str[i] == '\t' || str[i] == '\r'
								||str[i] == '\f' || str[i] == '\v'))
		++i;
	return (i);
}

int str_chars_not_in_base(char *str, char *base)
{
	int i, j, starting_index;

	starting_index = get_starting_index(str);
	i = starting_index;
	while(str[i])
	{
		if (first_char_is_math_sign(i, starting_index, str))
			i++;
		j = 0;
		while(base[j] && str[i] != base[j])
		{
			if ((!current_char_is_not_math_sign(str[i])) && (i > starting_index))
				return 1;
			else if(str[i] != base[j])
				j++;
		}
		if (str[i] != base[j])
			return 1;
		i++;
	}
	return 0;
}

int get_nb(char c, char *base)
{
	int i ;

	i = 0;
	while(base[i] && base[i] != c)
		i++;
	return (i);
}

int is_valid_parameters(char *str, char *base, int base_length)
{
	return (base_length != 0 && (!str_chars_not_in_base(str, base) != 0));
}

int first_char_is_math_sign(int i, int starting_index, char *str)
{
	if((str[i] == '+' || str[i] == '-') && i == starting_index)
		return 1;
	return 0;
}

int current_char_is_not_math_sign(char c)
{
	if (c != '+' || c != '-')
		return 1;
	return 0;

}

int ft_atoi_base(char *str, char *base)
{
	int starting_index, i, sign, res, base_length;

	sign = 1;
	res = 0;
	base_length = get_base_length(base);

	if (!is_valid_parameters(str, base, base_length))
		return (0);
	starting_index = get_starting_index(str);
	i = starting_index;

	while(str[i])
	{
		if (str[i] == '-')
			sign = -1;
		else if (str[i] != '+')
			res = res * base_length + get_nb(str[i], base);
		i++;
	}

	return sign * res;

}

int main()
{
	printf("%d", ft_atoi_base("   -42 23", "0123456789"));
	return 0;
}
