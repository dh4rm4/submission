#include <stdio.h>

void ft_sort_integer_table(int *tab, int size)
{
	int i, j;
	int tmp;

	i = 0;
/*
	 If you only give the value of 'j' here, it will never go back to zero
	 And you will only go one time in the second loop
	 So ou need to set the value of j inside the first for loop
	 --
	 It works in the 'for loop', because it already reset 'j' to 0
	 every time loop
	 j = 0;
*/
	while(i < size -1)
	{
//		 You should put it here, so every time you loop on i: you reset j to 0.
//       If not, after the first time you go inside the second loop
//       j will alway be "under size - i - 1", so you will never go
//       again inside that loop
		j = 0;
		while(j < size - i - 1)
		{
			if (tab[j] > tab[j +1])
			{
				tmp = tab[j];
				tab[j] =tab[j + 1];
				tab[j + 1] = tmp;
			}
			j++;
		}
		i++;
	}
/*
	for (i = 0; i < size-1; i++)
	{
		for (j = 0; j < size-i-1; j++)
		{
			if (tab[j] > tab[j+1])
			{
				tmp = tab[j];
				tab[j] =tab[j + 1];
				tab[j + 1] = tmp;
			}
		}
	}
*/
}

int main()
{
	int tab[5];
	int size;

	tab[0] = 3;
	tab[1] = 2;
	tab[2] = 1;
	tab[3] = 8;
	tab[4] = '\0';
	size = 4;
	ft_sort_integer_table(tab, size);

	int i = 0;
	while(i < size)
	{
		printf("%d", tab[i]);
		i++;
	}


}
