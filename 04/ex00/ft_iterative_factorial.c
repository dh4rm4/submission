#include <stdio.h>

int ft_iterative_factorial(int nb)
{
	int factorial;
	int i;

	if (nb < 0)
	{
		return (0);
	}

	i = 1;
	factorial = 1;
	while(i <= nb)
	{
		factorial = factorial * i;
		i++;

	}

	return factorial;
}



int main()
{
	int nb;
	nb = 5;
	printf("%d\n", ft_iterative_factorial(nb));
	return (0);

}
