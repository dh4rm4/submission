#include <unistd.h>
void ft_putchar(int i, int j)
{
	char num[8];
	num[0] = i/10 + '0';
	num[1] = i%10 + '0';
	num[2] = ' ';
	num[3] = j/10 + '0';
	num[4] = j%10 + '0';
	num[5] = ',';
	num[6] = ' ';
	num[7] = '\0';
	write(1, num, sizeof(num));

}


void ft_print_comb2(void)
{
	int i, j;
	i = 0;
	j = 1;
	while(i <= 98)
	{
		while(j <= 99)
		{
			ft_putchar(i, j);
			j++;
		}
		i++;
		j = i + 1;
	}

}


int main()
{
	ft_print_comb2();
	return (0);

}
