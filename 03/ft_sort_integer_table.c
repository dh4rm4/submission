#include <stdio.h>

void ft_sort_integer_table(int *tab, int size)
{
	int i, j;
	int tmp;

	i = 0;
	j = 0;

	while(i < size -1)
	{
		while(j < size - i - 1)
		{
			if (tab[j] > tab[j +1])
			{
				tmp = tab[j];
				tab[j] =tab[j + 1];
				tab[j + 1] = tmp;
			}
			j++;
		}
		i++;
	}

	/*for (i = 0; i < size-1; i++)
		for (j = 0; j < size-i-1; j++)
			if (tab[j] > tab[j+1])
			{
				tmp = tab[j];
				tab[j] =tab[j + 1];
				tab[j + 1] = tmp;
				}*/

}

int main()
{
	int tab[5];
	int size;

	tab[0] = 3;
	tab[1] = 2;
	tab[2] = 1;
	tab[3] = 8;
	tab[4] = '\0';
	size = 4;
	ft_sort_integer_table(tab, size);

	int i = 0;
	while(i < size)
	{
		printf("%d", tab[i]);
		i++;
	}


}
