#include <unistd.h>
#include <stdlib.h>
#include <math.h>

void ft_putstr(char *str)
{
	write(1, str, sizeof(str));
}

int get_num_length(int n)
{
	int len;

	len = 0;
	while(n > 0)
	{
		n /= 10;
		++len;
	}
	return (len);
}

void ft_putnbr(int nb)
{
	int num_len;
	int j;
	char *num_in_char;

	num_len = get_num_length(nb);
	num_in_char = malloc(sizeof(char) * (num_len + 1));
	num_in_char[num_len] = '\0';

	while( num_len >= 0)
	{
		num_in_char[num_len - 1] = (nb % 10) + 48;
		nb /= 10;
		--num_len;
	}
	ft_putstr(num_in_char);
}


int main()
{
	int nb;
	nb = 42;
	ft_putnbr(nb);
	return (0);
}
