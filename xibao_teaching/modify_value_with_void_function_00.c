#include <stdio.h>

void multiply_by_two(int *ptr_n)
{
	*ptr_n *= 2;
}

int main()
{
	int var;

	var = 21;
	multiply_by_two(&var);
	printf("%d\n", var);
	return(0);
}
