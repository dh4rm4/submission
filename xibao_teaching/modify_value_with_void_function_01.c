#include <stdio.h>

void multiply_by_two(int *ptr_n)
{
	*ptr_n *= 2;
}

int main()
{
	int var;
	int *ptr_var;

	var = 21;
	ptr_var = &var;

	multiply_by_two(ptr_var);

	printf("%d\n", var);
	return(0);
}
