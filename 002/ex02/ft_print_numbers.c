#include <unistd.h>

void ft_putchar(char i)
{
	write(1, &i, 1);
}

void ft_print_numbers(void)
{
	char i;

	i = '0';

	while(i <= '9')
	{
		ft_putchar(i++);
	}
}

int main(void)
{
	ft_print_numbers();
	return (0);
}
