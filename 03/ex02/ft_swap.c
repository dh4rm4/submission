#include <stdio.h>

void ft_swap(int *a, int *b)
{
	int tmp;

	if (a != NULL && b != NULL)
	{
		tmp = *b;
		*b = *a;
		*a = tmp;
	}
}


int main()
{
	int c, d;
	void *a, *b;

	a = &c;
	b = &d;
	c = 42;
	d = 420;
	printf("zero: %d %d\n", c, d);
	ft_swap(a, b);
	printf("scd: %d %d\n", c, d);
	return (0);
}
